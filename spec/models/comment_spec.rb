require 'rails_helper'

RSpec.describe Comment, type: :model do

  describe 'associations and validastions' do
    it { should belong_to(:topic) }
    it { should belong_to(:comment) }
    it { should have_many(:replies).dependent(:destroy) }
    it { should validate_presence_of(:body) }
  end

end
