require 'rails_helper'

RSpec.describe Topic, type: :model do

  describe 'associations and validastions' do
    it { should have_many(:comments).dependent(:destroy) }
    it { should accept_nested_attributes_for(:comments) }
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:comments) }
  end

end
