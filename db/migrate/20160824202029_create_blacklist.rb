class CreateBlacklist < ActiveRecord::Migration[5.0]
  def change
    create_table :blacklists do |t|
      t.string :word
      t.string :regex_word
    end
  end
end
