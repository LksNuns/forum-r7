class RemoveBodyTopics < ActiveRecord::Migration[5.0]
  def change
    remove_column :topics, :body, :string
  end
end
