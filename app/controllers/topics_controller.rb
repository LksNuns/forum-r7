class TopicsController < ApplicationController
  before_action :set_topic, only: [ :show ]

  def index
    @topics = Topic.page params[:page]
  end

  def new
    @topic = Topic.new
    @message = @topic.comments.build
  end

  def create
    @topic = Topic.new(topic_params)

    if @topic.save
      redirect_to topics_path, notice: "Novo Tópico Criado"
    else
      render "new"
    end
  end

  def show
    @comment = Comment.new
  end

  private

  def topic_params
    params.require(:topic).permit(:title, :body, comments_attributes: [ :id, :body, :topic_id ])
  end

  def set_topic
    @topic = Topic.find(params[:id])
  end

end
