class CommentsController < ApplicationController


  def create
    @topic = Topic.find(params[:topic_id])
    @comment = @topic.comments.build(comment_params)

    if @comment.save
      redirect_to topic_path(@topic), notice: "Novo comentário."
    else
      redirect_to topic_path(@topic), notice: "Um erro ocorreu."
    end
  end


  private

  def comment_params
    params.require(:comment).permit(:body, :topic_id, :comment_id)
  end

end
