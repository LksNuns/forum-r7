# == Schema Information
#
# Table name: comments
#
#  id         :integer          not null, primary key
#  body       :text             not null
#  comment_id :integer
#  topic_id   :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_comments_on_comment_id  (comment_id)
#  index_comments_on_topic_id    (topic_id)
#
# Foreign Keys
#
#  fk_rails_6545a5f2bc  (comment_id => comments.id)
#

class Comment < ApplicationRecord
  belongs_to :topic
  belongs_to :comment, required: false
  has_many :replies, class_name: "Comment", foreign_key: "comment_id", dependent: :destroy

  validates :body, presence: true

  before_save :sanitize

  private

  def sanitize
    Blacklist.sanitize!(body)
  end

end
