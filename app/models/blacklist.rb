# == Schema Information
#
# Table name: blacklists
#
#  id         :integer          not null, primary key
#  word       :string
#  regex_word :string
#

class Blacklist < ApplicationRecord

  before_save :create_regex_word


  def self.sanitize!(text)
    self.all.each do |word|
      reg = Regexp.new(word.regex_word, "i")
      stars = "*"*(word.word.length)
      text.gsub!(reg, stars)
    end
    return text
  end

  private

  def create_regex_word
    letters_variation = {
      'a' => "[a@4]",
      'e' => "[e3]",
      'i' => "[i!|]",
      'o' => "[o0]"
    }

    self.regex_word = self.word
    letters_variation.each do |key, value|
      self.regex_word = self.regex_word.gsub(key, value)
    end
  end


end
