# == Schema Information
#
# Table name: topics
#
#  id         :integer          not null, primary key
#  title      :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Topic < ApplicationRecord

  has_many :comments, inverse_of: :topic, dependent: :destroy
  accepts_nested_attributes_for :comments

  validates :title, presence: true
  validates :comments, presence: true

end
